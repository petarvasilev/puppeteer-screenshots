const puppeteer = require('puppeteer');
const argum = require('argum');

var Screenshot = function(options) {
    var config;

    if (options === undefined)
    {
        config = {
            url:            argum.get('--url', true, false),
            path:           argum.get('--path', false, 'image.png'),
            viewportWidth:  parseInt(argum.get('--viewportWidth', false, 1280)),
            viewportHeight: parseInt(argum.get('--viewportHeight', false, 768)),
            mobile:         argum.get('--mobile', false, false),
            userAgent:      argum.get('--userAgent', false, false),
            pdf:            argum.get('--pdf', false, false),
            mediaTypePrint: argum.get('--mediaTypePrint', false, false),
            delay:          parseInt(argum.get('--delay', false, false)),
            hide:           argum.get('--hide', false, false),
            visibility:     argum.get('--visibility', false, false),
        };
    }
    else
    {
        config = {
            url:            argum.object(options, 'url', true, false),
            path:           argum.object(options, 'path', false, 'image.png'),
            viewportWidth:  parseInt(argum.object(options, 'viewportWidth', false, 1280)),
            viewportHeight: parseInt(argum.object(options, 'viewportHeight', false, 768)),
            mobile:         argum.object(options, 'mobile', false, false),
            userAgent:      argum.object(options, 'userAgent', false, false),
            pdf:            argum.object(options, 'pdf', false, false),
            mediaTypePrint: argum.object(options, 'mediaTypePrint', false, false),
            delay:          parseInt(argum.object(options, 'delay', false, false)),
            hide:           argum.object(options, 'hide', false, false),
            visibility:     argum.object(options, 'visibility', false, false),
        };
    }

    return (async function() {

        const browser = await puppeteer.launch({
            args: ['--lang=en-US,en']
        });
        const page = await browser.newPage();

        // set the media type
        if (config.mediaTypePrint)
        {
            await page.emulateMedia('print');
        }
        else
        {
            await page.emulateMedia('screen');
        }

        // set the user agent if one is provided
        if (config.userAgent)
        {
            await page.setUserAgent(config.userAgent);
        }

        if (config.mobile)
        {
            // set user agent to be as Chrome 60 for Android
            await page.setUserAgent('Mozilla/5.0 (Linux; Android 5.1; XT1039 Build/LPBS23.13-17.6-1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.116 Mobile Safari/537.36');
        }

        await page.goto(config.url, {"waitUntil" : "networkidle0", "timeout" : 45000}).catch(function(err) {
            console.log('Error while opening page.', err);
            process.exit(1);
        });

        await page.setViewport({width: config.viewportWidth, height: config.viewportHeight});

        if (config.mobile)
        {
            // set mobile viewport
            await page.setViewport({width: 320, height: 480});
        }
        else
        {
            await page.setViewport({width: parseInt(config.viewportWidth), height: parseInt(config.viewportHeight)}); // set view port size
        }

        if (config.hide)
        {
            var toHide = config.hide.split(',');

            await page.evaluate(function(toHide) {
                for(var i = 0; i < toHide.length; i++)
                {
                    var elements = document.querySelectorAll(toHide[i]);

                    if (elements)
                    {
                        for (var z = 0; z < elements.length; z++)
                        {
                            elements[z].style.display = 'none';
                        }
                    }
                }

                return true;
            }, toHide);
        }

        if (config.visibility)
        {
            var toMakeInvisible = config.visibility.split(',');

            await page.evaluate(function(toHide) {
                for(var i = 0; i < toHide.length; i++)
                {
                    var elements = document.querySelectorAll(toHide[i]);

                    if (elements)
                    {
                        for (var z = 0; z < elements.length; z++)
                        {
                            elements[z].style.visibility = 'hidden';
                        }
                    }
                }

                return true;
            }, toMakeInvisible);
        }

        if (config.pdf)
        {
            // save pdf
            await page.pdf({path: config.path, printBackground: true, displayHeaderFooter: true});
        }
        else
        {
            // save screenshot
            await page.screenshot({path: config.path, fullPage: true});
        }

        browser.close();
    })()
};

exports.init = function () {
    return new Screenshot();
};

exports.screenshot = function (options) {
    return new Screenshot(options);
};