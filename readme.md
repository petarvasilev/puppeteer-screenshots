# Puppeteer Screenshots

This package provides a simple way to take screenshots and PDFs of urls. 

This is a wrapper around Google's Puppeteer package.
 
This package is in Alpha. This package follows SemVer.

## Install

Install using

    npm i puppeteer-screenshots

## How to use

There is two ways to use `puppeteer-screenshots`, the first is by doing this in your app file

    require('puppeteer-screenshots').init();
    
that way you can call your program from the command line like this

    node app.js --url=https://www.google.com
    
and a screenshot will be made and into "image.png". You can optionally specify path with 
--path=/path/to/file.png.

The other way to use it is to do this

    var ps = require('puppeteer-screenshots');
    
    ps.screenshot({url: 'https://google.com', path: 'google.png'});
    
which will save a screenshot of Google in 'google.png'. Note that here we don't have the double hyphens
as when using from the command line.

## Options

The available options you can pass to 'puppeteer-screenshots' are as follows

    --url - (required) the url to take screenshot of 
    --path - (optional) the path to save the screenshot to, default is 'image.png' 
    --viewportWidth - (optional) the viewport width, default value is 1280 pixels
    --viewportHeight - (optional) the viewport height, default value is 768 pixels
    --userAgent - (optional) the user agent to use
    --mobile - (optional) if this is set to true it will use a mobile user agent and set the viewport width to 320px and the viewport height to 480px, note that this overrides viewportWidth, viewportHeight and userAgent
    --pdf - (optional) if true saves the screenshot as pdf
    --mediaTypePrint - (optional) if set emulates the media type as print
    --hide - (optional) a comma separated list of css selectors of elements which to hide using JavaScript, e.g if there are any pop-ups you want to hide
    --visibility (optional) a comma separated list of css selectors of elements which to hide using JavaScript this is different than `hide` in that it sets the visibility property to hidden rather than the display to none
    
## License

MIT License

Copyright 2020 [Petar Vasilev](https://www.petarvasilev.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.